import csv

# Global variables
EVENT_SAVE_NAME = 'Idle Apoc Balancing _ AquaAdventureSave (7).csv'
NUM_OF_PRODUCERS = 8
EVENT_TIME = 259200  # we use 259200 here because there are 259,200 seconds in 72 hours -- an event length
seconds_left = EVENT_TIME

list_entities = []
list_producers = []
list_balances = []
is_producing = []   # array of flags that signify if a resource is being produced


class Producer:

    def __init__(self,  name, time):
        self.name = name
        if time:
            self.time = int(time)
        self.level = 0
        self.levels = []

    def level_up(self):
        if self.can_upgrade():
            costs = self.get_next_costs()
            print("Balance before upgrade: " + str(list_balances))
            if costs:
                for cost in costs:    # for each item in the current list of outputs
                    for bal in list_balances:   # check to see if there's a matching item in the producer's current level
                        if bal.name == cost.name:   # if there is, check that the cost is less than the balance
                            bal += cost     # remember, costs are negative, so we add here
                            for idx, temp in enumerate(list_balances):   # then, look in the original list to find index
                                if bal.name in temp.name:                # of original amount
                                    list_balances[idx] = bal             # and overwrite it.

            print("Leveled up " + self.name + " at " + str(EVENT_TIME) + " seconds left.")
            print("Balance after upgrade: " + str(list_balances))
            self.level += 1
            return True
        else:
            return False

    def get_speed(self):
        speed = self.levels[self.level].drop1.amt / int(self.time)
        return speed

    def get_next_speed(self):
        if self.level == len(self.levels) - 1:
            return self.get_speed()
        else:
            speed = self.levels[self.level+1].drop1.amt / int(self.time)
            return speed

    def get_next_costs(self):
        if self.level == len(self.levels) - 1:  # no acceleration is there is no next level
            return [0.0]

        next_level = self.levels[self.level]
        costs = []

        # get list of costs for next level. this list either has 1 or 2 elements
        if hasattr(next_level, 'purchase1') and next_level.purchase1.name != "None":
            costs.append(next_level.purchase1)
        else:
            return float(0)  # if cost is free, there is 0 time to upgrade; IOW, prioritise free upgrades above all
        if hasattr(next_level, 'purchase2') and next_level.purchase2.name != "None":
            costs.append(next_level.purchase2)

        return costs

    def can_upgrade(self):
        costs = self.get_next_costs()
        canupgrade = True

        if self.level == len(self.levels) - 1:  # if at final level, we can't upgrade any further
            return False

        if not costs:       # if costs is empty, there are no costs to upgrade, so this must be upgradable
            return canupgrade

        for cost in costs:    # for each item in the current list of outputs
            for bal in list_balances:   # check to see if there's a matching item in the producer's current level
                if bal.name == cost.name:   # if there is, add them
                    # take last character of cost.name and use it as index in is_producing to check production
                    resource_idx = int(cost.name[len(cost.name) - 1:len(cost.name)]) - 1
                    if bal.amt < abs(cost.amt) or not is_producing[resource_idx]:
                        canupgrade = False

        return canupgrade

    def get_time_to_next_upgrade(self):
        costs = self.get_next_costs()
        current_production = get_production_rates()
        times = []

        if not costs:
            return 0

        # check how much of each cost is being produced per second currently, and calculate time to reach each cost
        for cost in costs:
            for prod in current_production:
                if prod.name == cost.name:
                    if prod.amt <= 0:
                        time = float("inf")
                        times.append(time)
                        break
                    time = (-cost.amt / prod.amt)
                    if time < 0:
                        time = float("inf")
                    times.append(time)
                    break

        max = -1

        # then, out of the times to reach each cost, find the largest -- that's the time to reach the next upgrade
        for time in times:
            if time > max:
                max = time

        return max

    def get_accel(self):
        # acceleration is (final speed - initial speed) / elapsed time
        if self.get_time_to_next_upgrade() == 0:
            return float("inf")
        return (self.get_next_speed() - self.get_speed()) / self.get_time_to_next_upgrade()

    def __str__(self):
        return self.name + " " + str(self.levels[self.level])


class Currency:     # update: this class was very much *not* unnecessary

    def __init__(self, name, amt):
        self.name = name
        self.amt = float(str(amt).replace(',', ''))

    def __str__(self):
        return str(self.amt) + " " + self.name

    def __add__(self, other):
        if hasattr(other, "name"):
            if other.name == self.name:
                return Currency(self.name, self.amt + other.amt)
            else:
                print('Error: Only ints or other Currency objects of same name can be added with a Currency')
                return
        if other is None:   # handles special edge case of Currency + None, which should be the original Currency
            return self
        try:
            return Currency(self.name, self.amt + other)
        except TypeError:
            print('Error: Only ints or other Currency objects of same name can be added with a Currency')
            return

    __radd__ = __add__
    __repr__ = __str__


class Level:

    def __init__(self, levelnum = 0, purchasetype1 = None, purchaseamount1 = 0, purchasetype2 = None, purchaseamount2 = 0,
                 bonustype1 = None, bonusamount1 = None, bonuskind = None, costtype1 = None, costamount1 = 0,
                 costtype2 = None, costamount2 = 0, droptype1 = None, dropamount1 = 0, droptype2 = None, dropamount2 = 0):
        self.info = []  # put the attributes into a list, for easier iterating when printing

        if levelnum:
            self.levelnum = levelnum        # store each attribute separately as well, so they can be easily accessed
            self.info.append(self.levelnum)
        if purchasetype1:
            self.purchase1 = Currency(purchasetype1, '-' + purchaseamount1)  # make costs negative for easy adding
            self.info.append(self.purchase1)
        if purchasetype2:
            self.purchase2 = Currency(purchasetype2, '-' + purchaseamount2)
            self.info.append(self.purchase2)
        if bonustype1:
            self.bonus1 = Currency(bonustype1, bonusamount1)
            self.info.append(self.bonus1)
        if bonuskind:
            self.bonuskind = bonuskind
            self.info.append(self.bonuskind)
        if costtype1:
            self.cost1 = Currency(costtype1, '-' + costamount1)
            self.info.append(self.cost1)
        if costtype2:
            self.cost2 = Currency(costtype2, '-' + costamount2)
            self.info.append(self.cost2)
        if droptype1:
            self.drop1 = Currency(droptype1, dropamount1)
            self.info.append(self.drop1)
        if droptype2:
            self.drop2 = Currency(droptype2, dropamount2)
            self.info.append(self.drop2)

    def __str__(self):
        ret = ""
        for s in self.info:
            if s is None:
                ret += 'null, '
            else:
                ret += str(s) + ', '
        return ret


'''
main reads the event-specific .csv file, and loads all producers/upgrades into a list.
Then, it runs the simulator to find the most optimal upgrade path.
'''
def main():
    # read event .csv file and import all producers and upgrades
    with open(EVENT_SAVE_NAME) as eventinfo:
        reader = csv.reader(eventinfo, delimiter=',')
        adding = False
        temp = None
        reader.__next__()   # skip first row
        curr_level = None
        for row in reader:  # iterate over
            if row[1]:      # row 1 has producer name, which is only true if it contains a value
                if adding:  # if adding is true here, we were following another producer and it just finished
                    list_entities.append(temp)  # therefore, add the old producer to list_entities and clear temp
                prodname = row[1]
                curr_level = 0
                temp = Producer(prodname, row[3])

                temp.levels.append(Level(curr_level, row[4], row[5], row[6], row[7], row[8], row[9], row[10], row[11], row[12],
                                     row[13], row[14], row[15], row[16], row[17], row[18]))
                curr_level += 1
                adding = True
            else:
                temp.levels.append(
                    Level(curr_level, row[4], row[5], row[6], row[7], row[8], row[9], row[10], row[11], row[12],
                          row[13], row[14], row[15], row[16], row[17], row[18]))
                curr_level += 1

    # add only producers to list_producers -- the rest will be used for upgrades and such
    for i in range(NUM_OF_PRODUCERS):
        list_producers.append(list_entities[i])

    # begin the main simulation part of the code
    global list_balances, EVENT_TIME, is_producing

    list_balances = [(Currency('EventCurrency1', 0)),  # list_balances is the amount of each resource we currently have
                    Currency('EventCurrency2', 0),   # this updates every tick (simulation ticks once per "second")
                    Currency('EventCurrency3', 0),
                    Currency('EventCurrency4', 0),
                    Currency('EventCurrency5', 0)]

    for i in list_balances:
        is_producing.append(False)

    test = "EventCurrency1"
    print(int(test[len(test) - 1:len(test)]))

    for sec in range(EVENT_TIME):
        # at the beginning of each second, check what resources we can produce and reset max_accel to 0
        check_producing()
        max_accel = 0

        # check over each producer in the list, find the one which will give the most depth gain per second, and focus
        # on upgrading them
        for prod in list_producers:
            if prod.get_accel() > max_accel and prod.can_upgrade():
                focused_prod = prod
                max_accel = prod.get_accel()

        if EVENT_TIME % 100 == 0:
            print(focused_prod)
            print(list_balances)

        # every second, try to level them up. if we can't, the level_up method won't let us, and we continue
        focused_prod.level_up()

        # update production rates every second, and then add the current production/second to the balances
        production = get_production_rates()
        for i in range(5):
            list_balances[i] += production[i];

        EVENT_TIME -= 1  # decrement time left by one second

        #print("Time left: " + str(EVENT_TIME) + " seconds")


def check_producing():
    production = get_production_rates()
    for idx, prod in enumerate(production):
        if prod.amt > 0:
            is_producing[idx] = True


def get_production_rates():
    list_outputs = [Currency('EventCurrency1', 0),  # list_outputs is the list of each resource's total output per sec
                    Currency('EventCurrency2', 0),
                    Currency('EventCurrency3', 0),
                    Currency('EventCurrency4', 0),
                    Currency('EventCurrency5', 0)]

    for prod in list_producers:
        prodlevel = prod.levels[prod.level]
        list_items = []
        isproducing = False

        if hasattr(prodlevel, 'drop1') and prodlevel.drop1.amt > 0:
            list_items.append(prodlevel.drop1)  # if drop1 is greater than 0, producer is past level 0 and is consuming
            isproducing = True
        if hasattr(prodlevel, 'drop2'):
            list_items.append(prodlevel.drop2)
        if hasattr(prodlevel, 'cost1') and isproducing:
            list_items.append(prodlevel.cost1)
        if hasattr(prodlevel, 'cost2') and isproducing:
            list_items.append(prodlevel.cost2)

        for out in list_outputs:    # for each item in the current list of outputs
            for item in list_items: # check to see if there's a matching item in the producer's current level
                if out.name == item.name:   # if there is, add them
                    out += (item.amt / prod.time)
                    for idx, temp in enumerate(list_outputs):   # then, look in the original list to find index of
                        if out.name in temp.name:               # original amount
                            list_outputs[idx] = out             # and overwrite it.

    return list_outputs


if __name__ == "__main__":
    main()